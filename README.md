# Curated RSS feeds

This is a curated, (increasingly) categorised collection of RSS feeds for good quality blogs. Subject matter is mostly focused on UK politics.

# Contributing

Make a merge request with additions to the 'urls' file in it, complete with categories. I'll take a look and probably merge it.

# Future plans

Generally interested in things that will make it easier to find and work with RSS feeds from a user's perspective. Make an issue and we can chat?

## RSS discovery

Writing something to crawl all sites for RSS feeds (as well as internet archives for historic ones) would be a neat precursor to writing a specialised RSS search engine. I'm aware that they exist, but there isn't an open source option for this and existing options seem not to be so great.
